# shop_HTML

#### 介绍
前后端分离，前端使用AJAX访问接口获取数据<br>
后台管理前端页面<br>
项目接口请看仓库 [shop](https://gitee.com/MarkPolaris/shop)<br>
相关内容介绍请看 [wiki](https://gitee.com/MarkPolaris/shop_HTML/wikis)

#### 软件架构
HTML,JS,jQuery<br>
bootstrap<br>
Apache服务器

#### 参与贡献

1. mark(本人)
2. qiang


#### 其他

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 个人博客 [博客园](https://www.cnblogs.com/MC-Curry/)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)